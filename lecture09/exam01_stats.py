#!/usr/bin/env python3

import requests

# Constants

URL = 'https://yld.me/raw/uA3'
MAX = 30

# Main Execution

## Fetch data
response = requests.get(URL)
data     = response.text.splitlines()
scores   = []

## Compute individual scores
for row in data:
    score = 0
    for point in row.split(','):
        score += float(point)
    scores.append(score)

## Summarize statistics
minimum = min(scores)
average = sum(scores)/len(scores)
maximum = max(scores)
print('MINIMUM: {:5.2f} ({:5.2f}%)'.format(minimum, minimum*100.0 / MAX))
print('AVERAGE: {:5.2f} ({:5.2f}%)'.format(average, average*100.0 / MAX))
print('MAXIMUM: {:5.2f} ({:5.2f}%)'.format(maximum, maximum*100.0 / MAX))
