#!/usr/bin/env python3

import collections
import glob
import os
import sys

# Globals

Years    = None
BarWidth = 20

# Functions

def usage(status=0):
    print('''Usage: {} [options] name
    -y    YEARS    Limit output to specified years 
    -b    WIDTH    Specify bar width (default: 20)
'''.format(os.path.basename(sys.argv[0])))
    sys.exit(status)

def load_data():
    names = collections.defaultdict(dict)
    for path in sorted(glob.glob('yob*.txt')):
        year = path[3:7]

        for line in open(path):
            name, gender, count = line.split(',')
            names[year][name] = names[year].get(name, 0) + int(count)

    return names

def dump_name(data, name, years=None, bar_width=BarWidth):
    for year, names in sorted(data.items()):
        if years and year not in years:
            continue

        total = sum(names.values())
        count = names[name]
        prcnt = names[name] * 100.0 / total
        bar   = 'x' * int(prcnt * bar_width * 10)

        print('{:4}\t{:5} ({:4.2f}%) {}'.format(year, count, prcnt, bar))

# Parse command-line options

args  = sys.argv[1:]
while args and args[0].startswith('-') and len(args[0]) > 1:
    arg = args.pop(0)
    if arg == '-y':
        Years = set(args.pop(0).split(','))
    elif arg == '-b':
        BarWidth = float(args.pop(0))
    else:
        usage(1)

if args:
    Name = args.pop(0)
else:
    usage(1)

# Main Execution

Names = load_data()
dump_name(Names, Name, Years, BarWidth)
