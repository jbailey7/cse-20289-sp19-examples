/* str_length */

#include "str.h"

size_t str_length(const char *s) {
    const char *c;
    for (c = s; *c; c++);
    return (c - s);
}
