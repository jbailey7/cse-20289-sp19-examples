#pragma once

#include <stdlib.h>

size_t str_length(const char *s);
