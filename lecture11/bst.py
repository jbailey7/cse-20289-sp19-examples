#!/usr/bin/env python3

import sys

# Classes

class Node(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left  = left
        self.right = right

    def __str__(self):
        return 'Node({},{},{})'.format(self.value, self.left, self.right)

class Tree(object):
    def __init__(self):
        self.root = None

    def insert(self, value):                # Discuss: Recursive helper
        self.root = self.insert_r(self.root, value)

    def insert_r(self, node, value):        # Discuss: Insertion
        if node is None:
            return Node(value)

        if value <= node.value:
            node.left  = self.insert_r(node.left, value)
        else:
            node.right = self.insert_r(node.right, value)

        return node

    def print(self):                        # Discuss: Traversal
        self.print_r(self.root)

    def print_r(self, node):                # Discuss: In-Order Traversal
        if not node:
            return

        self.print_r(node.left)
        print(node.value)
        self.print_r(node.right)

    def nodes(self):
        return self.nodes_r(self.root)

    def nodes_r(self, node):
        if not node:
            return

        yield from self.nodes_r(node.left)  # Discuss: yield from
        yield node.value                    #   for n in self.nodes_r(node.left): yield n
        yield from self.nodes_r(node.right)

# Main Execution

if __name__ == '__main__':
    tree = Tree()
    for number in map(int, sys.argv[1:]):
        tree.insert(number)

    #tree.print()
    print(list(tree.nodes()))
