#!/usr/bin/env python3

import sys

# Functions

def subsets(s):
    ''' Recursively generate all the subsets of s. '''
    return subsets_r('', s)

def subsets_r(s, c):
    ''' Recursively generate all the subset s from candidates c. '''
    if not c:
        yield s     # Base: No more candidates, so just return subset
    else:
        yield from subsets_r(s       , c[1:])   # Recursive: Don't take first candidate
        yield from subsets_r(s + c[0], c[1:])   # Recursive: Take first candidate

# Main Execution

if __name__ == '__main__':
    for subset in subsets(sys.argv[1:]):
        print(subset)
