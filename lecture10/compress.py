#!/usr/bin/env python3

import multiprocessing
import os
import sys

# Functions

def compress(path):
    return os.system('bzip2 -9 "{}"'.format(path))

# Main Execution

if __name__ == '__main__':
    paths = sys.argv[1:]

    # Iterative - sequential
    '''
    for path in paths:
        compress(path)
    '''

    # Functional - sequential
    '''
    list(map(compress, paths))
    '''

    # Functional - parallel
    pool = multiprocessing.Pool()
    list(pool.map(compress, paths))
