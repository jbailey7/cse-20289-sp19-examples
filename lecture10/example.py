#!/usr/bin/env python3


numbers = range(1, 11)

# Imperative
triples = []
for n in numbers:
    triples.append(n * 3)
print(triples)

# Map
triples = map(lambda n: n*3, numbers)
print(list(triples))

# Map - List comprehension
triples = [n*3 for n in numbers]
print(triples)

# Filter
odds = filter(lambda n: n % 2, triples)
print(list(odds))

# Filter - List Comprehension
odds = [t for t in triples if t % 2]
print(odds)

# Reduce - total of odd triples

from functools import reduce
total = reduce(lambda a, i: a + i, odds)
print(total)

# Reduce - List Comprehension (DNE)
total = sum(odds)
print(total)
